FROM golang:1.7.3 as builder
WORKDIR /go/src/MyProjects/Hello/
COPY . .
RUN go get
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o app .


From scratch
COPY --from=builder /go/src/MyProjects/Hello/app /usr/local/bin/app
ENV PORT 8080
EXPOSE 8080
ENTRYPOINT ["/usr/local/bin/app"]
